import WindowView from './window/window-view'
import WindowController from './window/window-controller'
import playerCtrl from './window/player/player-controller'

initGlobal()
initializeWindow()

function initGlobal () {
  const React = require('react')
  window.React = React
  window.playerCtrl = playerCtrl
  // window.WebTorrent = new WebTorrent()
}

function initializeWindow () {
  const windowView = new WindowView()
  const windowController = new WindowController(windowView)
  windowController.start()
}
