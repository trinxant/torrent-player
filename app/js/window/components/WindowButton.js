import React from 'react'

class WindowButton extends React.Component {

  render () {
    return (
      <a
        className={'win-btn ' + this.props.type}
        onClick={() => {
          this.props.handler(this.props.type)
        }} />
    )
  }
}

export default WindowButton
