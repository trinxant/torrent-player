import React from 'react'
import WindowButton from './WindowButton'

class WindowButtonsContainer extends React.Component {

  renderButton (type) {
    return <WindowButton
      type={type}
      handler={this.props.handler}
    />
  }
  render () {
    return (
      <div
        className='win-btns-container'
        >
        {this.renderButton('close')}
        {this.renderButton('minimize')}
        {this.renderButton('expand')}
      </div>
    )
  }
}

export default WindowButtonsContainer
