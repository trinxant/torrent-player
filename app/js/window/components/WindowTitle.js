import React from 'react'

class WindowTitle extends React.Component {

  render () {
    return (
      <p className='win-title'>
        {this.props.title}
      </p>
    )
  }
}

export default WindowTitle
