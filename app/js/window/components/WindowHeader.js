import React from 'react'
import WindowButtonsContainer from './WindowButtonsContainer'
import WindowTitle from './WindowTitle'

class WindowHeader extends React.Component {

  render () {
    return (
      <div>
        <WindowButtonsContainer handler={this.props.handler} />
        <WindowTitle title={this.props.title} />
        <p className='free-mode'>
          Free mode
        </p>
        <div className='search-popup'></div>
      </div>
    )
  }
}

export default WindowHeader
