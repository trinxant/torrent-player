import EventEmitter from 'events'
import ReactDOM from 'react-dom'
import WindowHeader from './components/WindowHeader'

class WindowHeaderView extends EventEmitter {

  constructor () {
    super()
    this._handleHeaderButtonPressed = (type) => {
      this.emit(type)
    }
  }

  renderHeader (title) {
    ReactDOM.render(
      <WindowHeader title={title} handler={this._handleHeaderButtonPressed} />,
      document.querySelector('.header')
    )
  }
}

export default WindowHeaderView
