import EventEmitter from 'events'
import ReactDOM from 'react-dom'

class MainFrameView extends EventEmitter {

  constructor () {
    super()
    this._mainFrameElement = document.querySelector('.main-frame')
  }

  renderMainFrame () {
    console.log('render main frame')
  }
}

export default MainFrameView
