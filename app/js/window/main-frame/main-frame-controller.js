import BrowserController from './browser/browser-controller'

class MainFrameController {
  constructor (mainFrameView) {
    this._mainFrameView = mainFrameView
    this._browserController = new BrowserController(this)
  }

  start () {
    this._mainFrameView.renderMainFrame()
    this._browserController.start()
  }
}

export default MainFrameController
