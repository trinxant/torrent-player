import TitlePageView from './title-page-view'
import moment from 'moment'
const shell = window.require('electron').shell

class TitlePageController {
  constructor (browserController) {
    this._browserController = browserController
    this._titleData = {}
    this._titlePageView = new TitlePageView()
  }
  start () {
    this._titlePageView.start()
    this._handleViewEvents()
  }
  show () {
    this._titlePageView.show()
  }
  hide () {
    this._titlePageView.hide()
  }
  loadTitleData (titleData) {
    this._titleData = titleData
    this._titleData.torrents.forEach((torrent, i) => {
      if (torrent.quality === '3D') this._titleData.torrents.splice(i, 1)
    })
    titleData.rating = Math.round(titleData.rating * 10) / 10
    if (titleData.trailer) {
      titleData.trailerEmbed = titleData.trailer.replace('watch?v=', 'embed/')
    }
    titleData.released = moment(titleData.released).format('YYYY MMMM')
    this._titlePageView.renderPage(titleData)
    this.show()
  }
  _handleViewEvents () {
    this._titlePageView.on('close', () => {
      this._close()
    })
    this._titlePageView.on('watch', (quality) => {
      window.playerCtrl.show()
      window.playerCtrl.startStream(this._getTorrentUrlByQuality(this._titleData, quality))
      this._close()
    })
    this._titlePageView.on('ytb-click', () => {
      console.log('titleData', this._titleData)
      shell.openExternal(this._titleData.trailer)
    })
  }

  _close () {
    this._browserController.showTitleList()
    this._browserController.updateScrollPos()
    this._titlePageView.close()
  }
  _getTorrentUrlByQuality (data, quality) {
    let output = ''
    data.torrents.forEach((torrent) => {
      if (torrent.quality === quality) {
        torrent.hash ?
          output = torrent.hash :
          output = torrent.url
      }
    })
    if (output === '') {
      output = data.torrents[1].url
    }
    return output
  }
}

export default TitlePageController
