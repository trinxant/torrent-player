import EventEmitter from 'events'
import htmlToElement from 'html-to-element'
import Mustache from 'mustache'
import $ from 'jquery'


class TitlePageView extends EventEmitter {
  constructor () {
    super()
    this._titlePageElement = $('.title-page')
    this._width = this._titlePageElement.width()
    this._height = this._titlePageElement.height()
    this._width = $('.browser').width()
    this._height = $('.browser').height()
    this._templates = {}
  }

  start () {
    this._loadTpl('title-page')
  }

  hide () {
    this._titlePageElement.hide()
  }

  close() {
    this.hide()
    this.stopTrailer()
  }

  stopTrailer() {
    document.getElementById('ytb-trailer').contentWindow.location.reload();
  }

  show () {
    this._titlePageElement.show()
    this._updateSizes()
  }

  renderPage (titleData) {
    let pageHTML = Mustache.render(this._templates.titlePage, titleData)
    this._titlePageElement.html('')
    this._titlePageElement.append(htmlToElement(pageHTML))
    this._setBackground(titleData.traktImages.background)
    this._setPosterImage(titleData.traktImages.poster)
    this._setViewElements()
    this._setEvents()
  }

  _loadTpl (name) {
    $.get(`assets/${name}.mst`, (template) => {
      this._templates.titlePage = template
    })
  }

  _setViewElements () {
    this._mainContentElement = this._titlePageElement.find('.main-content')
  }

  _setEvents () {
    this._setResizeEvents()
    $('.close-btn').click(() => {
      this.emit('close')
      $('.free-mode, .search-popup').show()
    })
    $('.watch-btn').click((e) => {
      const quality = e.currentTarget.attributes['data-quality'].nodeValue
      this.emit('watch', quality)
    })
    $('.title-page-tab').click((e) => {
      $('.info-content').hide()
      $('.title-page-tab').removeClass('active')
      $(e.currentTarget).addClass('active')
      const classe = '.' + e.currentTarget.attributes['data-content'].nodeValue
      if(classe == '.info-content-1') this.stopTrailer()
      $(classe).show()
    })
  }

  _setBackground (url) {
    this._loadImg(url, () => {
      this._titlePageElement.find('.blur-layer').css('background', 'url(' + url + ')')
      this._titlePageElement.find('.blur-layer').css('background-size', 'cover')
    })
  }

  _loadImg (url, callback) {
    var img = new Image()
    img.addEventListener('load', callback, false)
    img.src = url
  }

  _setPosterImage (url) {
    this._loadImg(url, () => {
      this._titlePageElement.find('.poster-background').css('background', 'url(' + url + ')')
      this._titlePageElement.find('.poster-background').css('background-size', 'cover')
    })
  }

  _setPosterHeight (height) {
    this._titlePageElement.find('img').height(height)
  }

  _setResizeEvents () {
    $(window).resize(() => this._updateSizes())
  }

  _updateSizes () {
    this._width = $('.browser').width()
    this._height = $('.browser').height()
    this._centerMainContent()
  }

  _centerMainContent () {
    let marginTop = (this._height - this._titlePageElement.find('.main-content').height()) / 2
    let marginLeft = (this._width - this._titlePageElement.find('.main-content').width()) / 2
    this._titlePageElement.find('.main-content').css('top', marginTop)
    this._titlePageElement.find('.main-content').css('left', marginLeft)
  }


}

export default TitlePageView
