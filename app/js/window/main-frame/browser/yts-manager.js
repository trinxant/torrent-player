import request from 'request'
import defaultYifiMovies from '../../../../assets/default-yifi-movies.json'

// PATHS
const listMoviesPATH = 'https://yts.ag/api/v2/list_movies.json?'

const YTSManager = {

  defaultYifiMovies () {
    return defaultYifiMovies
  },
  listMovies (parameters, callback) {
    let url = listMoviesPATH
    const keys = Object.keys(parameters)
    keys.forEach((parameter) => {
      url.concat(parameter + '=' + parameters[parameter])
    })
    request({
      url,
      callback
    })
  },
  getMovieById (movie, callback) {
    let url = listMoviesPATH + 'query_term=' + movie.ids.imdb
    request(
      url,
      callback
    )
  },
  getMoviesById (movies, callback) {
    let url = listMoviesPATH
    movies.forEach((movie) => {
      const urlAux = url + 'query_term=' + movie.ids.imdb
      request(
        urlAux,
        callback
      )
    })
  }
}
export default YTSManager
