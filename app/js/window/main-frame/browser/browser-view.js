import EventEmitter from 'events'
import delegate from 'delegate'
import request from 'request'
import htmlToElement from 'html-to-element'
import Mustache from 'mustache'
import genres from './genres.json'
import $ from 'jquery'

class BrowserView extends EventEmitter {

  constructor () {
    super()
    this._genres = genres
    this._loadingTitles = false
    this._browserElement = $('.browser')
    this._titleListElement = {}
    this._formElement = {}
    this._titlePageElement = {}
    this._templates = {}
  }

  start () {
    this._setViewElements()
    this._loadTemplates()
  }

  renderTitles (titles) {
    clearInterval(this._loadingInterval)
    this._loadingInterval = setInterval(() => {
      if (titles.length > 0) {
        this.renderTitle(titles[titles.length - 1])
        titles.pop()
      }
      if (titles.length === 0) {
        this._loadingTitles = false
        this.emit('titlesLoaded')
        clearInterval(this._loadingInterval)
        this._setScrollEvents()
        this._titleListElement.childNodes.forEach((child) => {
          child.classList.remove('popin')
        })
      }
    }, 50)
  }

  renderTitle (title) {
    let titleHTML = Mustache.render(this._templates.title, {title})
    const titleElem = htmlToElement(titleHTML)
    this._titleListElement.appendChild(titleElem)
    this._adaptSizes()
  }

  clearTitleList () {
    $('.browser-message').html('')
    $(this._titleListElement).html('')
  }

  hideTitleList () {
    this._titleListElement.style.display = 'none'
  }

  showTitleList () {
    this._titleListElement.style.display = 'block'
  }

  notFound () {
    this.hideLoader()
    $('.browser-message').html('<h1>No results found.<h1>')
  }

  setScrollPos (pos) {
    $('.main-frame').scrollTop(pos)
  }

  hideLoader () {
    this._loaderElement.style.display = 'none'
  }

  hidePopup () {
    $('.search-popup').addClass('hidden')
  }

  showPopup () {
    $('.search-popup').removeClass('hidden')
  }

  showLoader () {
    this._loaderElement.style.display = 'block'
    this._centerLoader()
    this._adaptSizes()
  }

  resetPopup () {
    $('.search-popup').find('input').val('')
    $('.search-popup').find('.popup-genre.active').removeClass('active')
  }

  _getGenres (callback) {
    request('https://api.trakt.tv/genres/movies', (error, response, body) => {
      if (error) {
        console.log(error)
      } else {
        this._genres = JSON.parse(body)
        callback()
      }
    })
  }

  _setViewElements () {
    this._setLoader()
    this._setUpArrow()
    this._setFormElement()
    this._setTitleListElement()
    this._setTitlePageElement()
    this._setSearchPopup()
  }

  _setLoader () {
    this._loaderElement = htmlToElement("<div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>")
    this._browserElement.append(this._loaderElement)
  }

  _setUpArrow () {
    this._upArrow = htmlToElement("<i class='arrow-up fa fa-arrow-circle-o-up' aria-hidden='true'></i>")
    this._browserElement.append(this._upArrow)
  }

  _setFormElement () {
    this._formElement = htmlToElement("<div class='search-form'></div>")
    this._browserElement.append(this._formElement)
  }
  _setTitleListElement () {
    this._titleListElement = htmlToElement("<div class='title-list'><h1 class='title-list-message'></h1</div>")
    this._browserElement.append(this._titleListElement)
  }
  _setTitlePageElement () {
    this._titlePageElement = htmlToElement("<div class='title-page'></div>")
    this._browserElement.append(this._titlePageElement)
  }

  _setSearchPopup () {
    // this._getGenres(() => {
    let html =
      `<div class='popup-section popup-top'>
        <div class='search-input'>
          <i class='fa fa-search' aria-hidden='true'></i>
          <input type='text' name='lname'>
        </div>
       </div>
       <div class='popup-section popup-mid'>`
    this._genres.forEach((genre) => {
      html += `<div class='popup-genre ${genre.slug}' id='${genre.slug}'><h5>${genre.name}</h5></div>`
    })
    html += '<div class="search-btn">Search<div></div>'
    $('.search-popup').append(html)
    // })
    this._setEvents()
  }

  _loadTemplates () {
    this._loadTpl('title')
  }

  _loadTpl (name) {
    $.get(`assets/${name}.mst`, (template) => {
      this._templates[name] = template
    })
  }

  _setEvents () {
    this._setResizeEvents()
    this._setTitleListEvents()
    this._setArrowUpEvent()
    this._setSearchPopupEvents()
    $('.free-mode').on('click', (e) => {
      $(e.target).toggleClass('active')
      this.emit('freeMode')
    })
  }

  _setTitleListEvents () {
    delegate(this._titleListElement, '.tl-item', 'click', (e) => {
      this.emit('titleClick', e.delegateTarget.id, $('.main-frame').scrollTop())
    }, false)
  }

  _setResizeEvents () {
    $(window).resize(() => this._adaptSizes())
  }

  _setArrowUpEvent () {
    $('.arrow-up').on('click', () => {
      $('.arrow-up').removeClass('active')
      $('.title-list').animate({ scrollTop: '0px' })
    })
  }

  _setScrollEvents () {
    $('.title-list').on('scroll', () => {
      if ($('.title-list').scrollTop() > 100) {
        $('.arrow-up').addClass('active')
      }
      if ($('.title-list').scrollTop() === 0) {
        $('.arrow-up').removeClass('active')
      }
      console.log($('.title-list').scrollTop(), $('.browser').height(), $('.title-list')[0].scrollHeight)
      if ($('.title-list').scrollTop() + $('.browser').height() >= $('.title-list')[0].scrollHeight) {
        if (!this._loadingTitles) {
          this._loadingTitles = true
          this.emit('bottom')
        }
      }
    })
  }

  _setSearchPopupEvents () {
    $('.search-popup').delegate('.popup-genre', 'click', (e) => {
      e.currentTarget.classList.toggle('active')
    })
    $('.search-popup').delegate('.search-btn', 'click', () => {
      this._emitSearch()
    })
    document.addEventListener('keydown', (event) => {
      if (event.which === 13) this._emitSearch()
    })
  }

  _emitSearch () {
    let genres = $('.search-popup').find('.popup-genre.active').map(function () { return this.id }).get()
    this.emit('search', {
      query: $('.search-popup').find('input')[0].value,
      genres: genres.join()
    })
    $('.search-popup').find('input').val('')
    this.resetPopup()
  }

  _adaptSizes () {
    this._width = $('.browser').width()
    this._height = $('.browser').height()
    this._itemWidth = $('.tl-item').width()
    this._adaptTitlesSize()
    this._centerLoader()
  }

  _adaptTitlesSize () {
    const m = (8 - 4) / (1024 - 400)
    const numCols = Math.round(this._width * m)
    const titleWidth = (this._width / numCols)
    $('.tl-item').width(titleWidth + 'px')
    $('.tl-item').height((titleWidth * 345 / 230) + 'px')
  }

  _centerLoader () {
    const spinner = this._browserElement.find('.spinner')
    const x = (this._width / 2) - (spinner.width() / 2)
    let y = $('.title-list').height()
    if (y === 0) {
      y = $('.browser').height()
    }
    spinner.css('left', x)
    spinner.css('top', y - spinner.height() - 40)
  }

}

export default BrowserView
