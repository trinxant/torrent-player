import Trakt from 'trakt.tv'
import request from 'request'

const trakt = new Trakt({
  client_id: 'e87bd520f149e6c76a2045aecb2f634e6f5f043a050833c058c0ffce299dd78f',
  client_secret: '59babbf88db1b10f10e3ed8a5aac0bf8e91b9d6c9dc9a32154f258f98c1d2f05',
  redirect_uri: null,
  api_url: null,
  pagination: true,
  plugins: {
    images: require('trakt.tv-images')
  },
  options: {
    images: {
      fanartApiKey: '5419e383fc5aff2c07a8365c04345d28',
      tvdbApiKey: '7D8BA4B8F93A3E7F',
      tmdbApiKey: 'ecfd28cb136fd1b082e8dc6ddcbbcedb',
      omdbApiKey: '3a70a17',
      smallerImages: false
    }
  }
})

const sampleParams =
  {
    query:  "bick buck bunny",
    genres: "animation",
    page:   1,
    limit:  80,
    type:   "movie"
  };

const TraktManager = {
  listPopularMovies (page, limit, callback) {
    trakt.movies.popular({
      page: page,
      limit: limit,
      extended: 'full'
    }).then((movies) => {
      callback(movies)
    })
  },
  getImagesById (ids, callback) {
    trakt.images.get({
      imdb: ids.imdb,
      type: 'movie'
    })
      .then((images) => {
        callback(images)
      })
      .catch(() => {
        // console.error('error geting trakt images', ids)
        // callback()
      })
  },
  searchMovies (params, callback) {
    params.extended = 'full'
    trakt.search.text(params).then(movies => {
      callback(movies)
    })
  }
}

export default TraktManager
