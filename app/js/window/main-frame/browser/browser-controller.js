import YTSManager from './yts-manager'
import TraktManager from './traktTvManager'
import BrowserView from './browser-view'
import legalContent from '../../../../assets/legal-content.json'
import TitlePageController from '../titlePage/title-page-controller'
import $ from 'jquery'

class BrowserController {
  constructor (mainFrameController) {
    this._mainFrameController = mainFrameController
    this._pageToLoad = 1
    this._listingType = 'popular'
    this._elementsPerPage = 50
    this._lastTopScroll = 0
    this._titlePageController = {}
    this._failedMovies = []
    this._loadedTitles = []
    this._browserView = new BrowserView()
    this._selectedTitle = null
    this._loadingTitles = false
    this._defaultYifiParameters = {
      query_term: 'nemo'
    }
  }

  start () {
    this._browserView.start()
    this._titlePageController = new TitlePageController(this)
    this._titlePageController.start()

    this._listingType == 'popular' ?
      this._loadPopularTitles() :
      this._loadLegalContent()
    this._handleViewEvents()
  }

  showTitleList () {
    this._browserView.showTitleList()
  }

  updateScrollPos () {
    this._browserView.setScrollPos(this._lastTopScroll)
  }

  _loadYifyMovies (movies) {
    movies.forEach((movie) => {
      YTSManager.getMovieById(movie, (err, data) => {
        if (err) {
          console.log('error getting yifi movie')
        } else {
          this._count++
          let status = this._checkTitleStatus(data)
          let yifyData = this._arrangeTitleData(status, data)
          if (yifyData) {
            movie.torrents = yifyData.torrents
            movie.yifyImgs = {
              'background_image': yifyData.background_image,
              'background_image_original': yifyData.background_image_original,
              'small_cover_image': yifyData.small_cover_image,
              'medium_cover_image': yifyData.medium_cover_image,
              'large_cover_image': yifyData.large_cover_image
            }
            this._currentTitles.push(movie)
            this._loadedTitles.push(movie)
          } else {
            if (status === 0) {
              this._failedMovies.push(movie)
              console.log('error getting this movie yify data: ' + movie.title)
            }
            if (status === 1) {
              console.log('this movies is not in the yify database: ' + movie.title)
              /* TraktManager.getImagesById(movie.ids, (imgs) => {
                console.log(JSON.stringify(imgs))
              })*/
            }
          }
        }
        if (this._count === movies.length) {
          if (this._currentTitles.length === 0) {
            this._pageToLoad++
            switch (this._listingType) {
              case 'search':
                this._searchTitles(this._searchParams)
                break
              case 'popular':
                this._loadPopularTitles()
                break
            }
          }
          if(this._listingType == 'legal') return
          this._browserView.renderTitles(this._currentTitles.slice(0, this._currentTitles.length))
          this._browserView.hideLoader()
        }
      })
    })
  }

  _loadPopularTitles () {
    this._loadingTitles = true
    this._listingType = 'popular'
    this._browserView.showLoader()
    TraktManager.listPopularMovies(this._pageToLoad, this._elementsPerPage, (movies) => {
      if(this._listingType != 'popular') return
      this._count = 0
      this._currentTitles = []
      this._failedMovies = []
      this._loadYifyMovies(movies)
    })
  }

  _loadLegalContent () {
    this._browserView.hideLoader()
    this._currentTitles = legalContent.slice(0)
    this._loadedTitles = legalContent.slice(0)
    this._failedMovies = []
    let titlesCoppy = []
    titlesCoppy = titlesCoppy.concat(this._currentTitles)
    this._browserView.renderTitles(titlesCoppy)
  }

  _searchTitles (params) {
    this._listingType = 'search'
    this._notOnYifyCount = 0
    this._loadingTitles = true
    this._browserView.showLoader()
    params.page = this._pageToLoad
    params.limit = 80
    params.type = 'movie'
    this._searchParams = params
    TraktManager.searchMovies(params, (movies) => {
      if(this._listingType != 'search') return
      if (movies.length === 0) {
        this._browserView.notFound()
      }
      this._count = 0
      this._currentTitles = []
      this._failedMovies = []
      let moviesAux = []
      movies.forEach(movie => moviesAux.push(movie.movie))
      // console.log(JSON.stringify(moviesAux))
      this._loadYifyMovies(moviesAux)
    })
  }

  _handleViewEvents () {
    this._browserView.on('freeMode', () => {
      this._browserView.clearTitleList()
      this._loadedTitles = []
      if (this._listingType != 'legal') {
        this._listingType = 'legal'
        this._loadLegalContent()
      } else {
        this._listingType = 'popular'
        this._loadPopularTitles()
      }
    })
    this._browserView.on('titleClick', (titleId, scrollTop) => {
      this._lastTopScroll = scrollTop
      this._selectedTitle = this._getTitleByID(titleId)
      if (this._listingType != 'legal') {
        this._selectedTitle.torrents.forEach((torrent, i) => {
          this._selectedTitle.torrents[i].disabled = 'disabled'
        })
      }
      if (this._selectedTitle.traktImages) {
        this._titlePageController.loadTitleData(this._selectedTitle)
        this._browserView.hideTitleList()
        $('.free-mode, .search-popup').hide()
      } else {
        TraktManager.getImagesById(this._selectedTitle.ids, (images) => {
          images ?
            this._selectedTitle.traktImages = images :
            this._selectedTitle.traktImages = this._selectedTitle.yifyImgs
          this._titlePageController.loadTitleData(this._selectedTitle)
          this._browserView.hideTitleList()
          $('.free-mode, .search-popup').hide()
        })
      }
    })
    this._browserView.on('bottom', () => {
      if (this._loadingTitles === false) {
        this._pageToLoad++
        switch (this._listingType) {
          case 'search':
            this._searchTitles(this._searchParams)
            break
          case 'popular':
            this._loadPopularTitles()
            break
          case 'legal':

            break
        }
      }
    })
    this._browserView.on('titlesLoaded', () => {
      this._loadingTitles = false
    })
    this._browserView.on('search', (params) => {
      this._browserView.clearTitleList()
      this._loadedTitles = []
      this._pageToLoad = 1
      this._searchParams = params
      this._searchTitles(params)
      this._browserView.showTitleList()
      this._titlePageController.hide()
    })
  }

  _getTitleByID (titleId) {
    let output = {}
    this._loadedTitles.forEach((title) => {
      if (String(title.ids.imdb) === titleId) {
        output = title
      }
    })
    return output
  }

  _checkTitleStatus (data) {
    if (data.statusCode === 200) {
      if (JSON.parse(data.body).data.movie_count === 0) {
        return 1
      } else {
        return 2
      }
    } else {
      return 0
    }
  }

  _arrangeTitleData (status, data) {
    if (status === 2) {
      if (!JSON.parse(data.body).data.movies) return null
      return JSON.parse(data.body).data.movies[0]
    } else {
      return null
    }
  }

}

export default BrowserController
