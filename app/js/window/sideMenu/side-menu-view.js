import EventEmitter from 'events'

class SideMenuView extends EventEmitter {

  constructor () {
    super()
    this._menuElement = document.querySelector('.side-menu')
  }

  renderMenu (title) {
    console.log('render menu', title)
  }
}

export default SideMenuView
