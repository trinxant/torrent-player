import strings from '../../../assets/strings'

class SideMenuController {
  constructor (sideMenuView) {
    this._sideMenuView = sideMenuView
  }

  start () {
    this._sideMenuView.renderMenu(strings.appName)
  }
}

export default SideMenuController
