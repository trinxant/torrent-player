(() => {
  require('events').EventEmitter.prototype._maxListeners = Infinity
  const fs = require('fs')
  const http = require('http')
  const EventEmitter = require('events').EventEmitter
  const networkAddress = require('network-address')
  var WebTorrent = require('webtorrent')
  class TorrentManager extends EventEmitter {

    constructor () {
      super()
      this._client = new WebTorrent()
      // this._downloadSubtitles('http://dl.opensubtitles.org/en/download/src-api/vrf-19db0c5a/sid-AbM9ajETAqEjvBUoJDXknp5T5J8/filead/1954918660')
      this._server = null
      this._serverInfo
      this._sampleUri = 'https://yts.ag/torrent/download/55C787FD5254EDF1209E7F9CFF5CB8F743DC6D92'
      this._torrent = null
    }

    startTorrent (hash, callback) {
      console.log(hash);
      if (this._client.get(hash)) {
        callback(this._client.get(hash))
        this._setMp4Index(this._client.get(hash))
        callback(this._client.get(hash))
        return
      }
      this._client.add(hash, (torrent) => {
        this._torrent = torrent
        console.log('Client is downloading:', torrent)
        this._setMp4Index(torrent)
        var interval = setInterval(() => {
          // window.playerCtrl._playerView.adaptPlayerSize()
          console.log('Progress: ' + (torrent.progress * 100).toFixed(1) + '%' + '  -  Speed: ' + torrent.downloadSpeed + '  -  Peers: ' + torrent.numPeers)
        }, 1000)
        torrent.on('done', function () {
          console.log('done', torrent)
          clearInterval(interval)
        })
        callback(torrent)
      })
    }

    getTorrent () {
      return this._torrent
    }

    startServer (_torrent) {
      const torrent = _torrent
      if (torrent.ready) {
        this.startServerFromReadyTorrent(torrent)
      } else {
        torrent.once('ready', () => {
          this.startServerFromReadyTorrent(torrent)
        })
      }
    }

    startServerFromReadyTorrent (torrent, cb) {
      if (this._server) {
        this._server.close()
      }

      // start the streaming torrent-to-http server
      this._server = torrent.createServer()
      this._server.listen(0, () => {
        const port = this._server.address().port
        const urlSuffix = ':' + port
        const info = {
          torrentKey: torrent.key,
          localURL: 'http://localhost' + urlSuffix,
          networkURL: 'http://' + networkAddress() + urlSuffix
        }
        this._serverInfo = info
        this.emit('server-ready', info)
        console.log('cast info', this._serverInfo)
        // ipc.send('wt-server-running', info)
        // ipc.send('wt-server-' + torrent.infoHash, info)
      })
    }

    _downloadSubtitles (url) {
      const file = fs.createWriteStream('subtitles.srt')
      http.get(url, (response) => {
        console.log(response)
        response.pipe(file)
      })
    }

    _setMp4Index (torrent) {
      const files = torrent.files
      for (let i = 0; i < files.length; i++) {
        if (files[i].name.endsWith('.mp4') || files[i].name.endsWith('.mkv')) {
          torrent.mp4Index = i
        }
      }
    }
  }
  window.torrentManager = new TorrentManager()
})()
