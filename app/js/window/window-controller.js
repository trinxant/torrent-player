import strings from '../../assets/strings'
const remote = window.require('electron').remote
import SideMenuController from './sideMenu/side-menu-controller'
import SideMenuView from './sideMenu/side-menu-view'
import MainFrameController from './main-frame/main-frame-controller'
import MainFrameView from './main-frame/main-frame-view'

class WindowController {

  constructor (windowView) {
    this._windowView = windowView
    this._sideMenuController = new SideMenuController(new SideMenuView())
    this._mainFrameController = new MainFrameController(new MainFrameView())
    window.playerCtrl.start()
  }

  start () {
    this._setWindowView()
    this._windowView.renderHeader(strings.appName)
    this._sideMenuController.start()
    this._mainFrameController.start()
  }

  _setWindowView () {
    this._setHeader
    const window = remote.getCurrentWindow()
    this._windowView.on('close', () => {
      window.close()
    })
    this._windowView.on('minimize', () => {
      window.minimize()
    })
    this._windowView.on('expand', () => {
      if (!window.isMaximized()) {
        window.maximize()
      } else {
        window.unmaximize()
      }
    })
  }

}

export default WindowController
