(() => {
  require('events').EventEmitter.prototype._maxListeners = Infinity
  const fs = require('fs')
  const request = require('request')
  const EventEmitter = require('events').EventEmitter
  const OpenSubs = require('opensubtitles-api')
  const srt2vtt = require('srt-to-vtt')
  const remote = window.require('electron').remote
  const app = remote.app

  class SubsManager extends EventEmitter {

    constructor () {
      super()
      this._setUp()
    }

    _setUp () {
      this._OpenSubtitles = new OpenSubs('TFG ls26104')
      this._OpenSubtitles.login()
        .then(res => {
          this._OSToken = res.token
        })
        .catch(err => {
          console.log(err)
        })
    }

    findSubtitles (params, callback) {
      this._OpenSubtitles.search(params)
        .then(subtitles => {
          callback(subtitles)
          this._subtitles = subtitles
        })
    }

    downloadSubtitles (lang) {
      let receivedBytes = 0
      let totalBytes = 0

      let req = request({
        method: 'GET',
        uri: this._subtitles[lang].url
      })
      const path = app.getPath('appData') + '/subtitles-' + lang
      let out = fs.createWriteStream(path + '.srt')
      req.pipe(out)

      req.on('response', (data) => {
        console.log(data.headers['content-length'])
        totalBytes = parseInt(data.headers['content-length'])
      })

      req.on('data', (chunk) => {
        receivedBytes += chunk.length
        this.showProgress(receivedBytes, totalBytes)
      })

      req.on('end', () => {
        console.log('File succesfully downloaded')
        const outvtt = fs.createWriteStream(path + '.vtt').on('finish', () => {
          this.emit('subtitles-ready', {lang: lang, path: path + '.vtt'})
        })
        fs.createReadStream(path + '.srt')
          .pipe(srt2vtt())
          .pipe(outvtt)
      })
    }

    showProgress (received, total) {
      var percentage = (received * 100) / total
      console.log(percentage + '% | ' + received + ' bytes out of ' + total + ' bytes.')
    }

  }
  window.subsManager = new SubsManager()
})()
