import EventEmitter from 'events'
import playerCtrl from './player-controller'

class CastManager extends EventEmitter {

  constructor () {
    super()
    this._chromecasts = window.chromecasts
    this._ccPlayer = null
    this._ccIndex = 0
  }

  start () {
    this._setupChromecast()
  }

  _setupChromecast () {
    if (this._chromecasts.players.length > 0) {
      this._ccPlayer = this._chromecasts.players[0]
      console.log(this._ccPlayer)
    }
    window.chromecasts.on('update', (player) => {
      if (this._ccPlayer === null) this._ccPlayer = player
      console.log(this._ccPlayer)
      this.emit('new-device', player)
    })
  }

  play (url, title, seconds = 0, deviceId) {
    console.log(url, title, deviceId)
    if (this._ccPlayer.status(() => { return })) this._ccPlayer.stop()
    this._ccPlayer = this._chromecasts.players[deviceId]
    if(seconds < 0) seconds = 0
    this._ccPlayer.play(url, {
      type: 'video/mp4',
      title: title,
      seek: seconds
    }, () => {
      this._ccPlayer.on('status', status => {
        playerCtrl.updateCastStatus(status)
      })
      if (this._castInterval) clearInterval(this._castInterval)
      this._castInterval = setInterval(() => {
        this._ccPlayer.status(this.handleStatus)
      }, 1000)
    })
  }

  stop (cb) {
    this._ccPlayer.stop(playerCtrl.stopCasting)
    clearInterval(this._castInterval)
  }

  pause () {
    this._ccPlayer.pause()
  }

  resume () {
    this._ccPlayer.resume()
  }

  seek (seconds, cb) {
    this._ccPlayer.seek(seconds, cb)
  }

  getDevices () {
    return this._chromecasts.players
  }

  handleStatus (err, status) {
    if (err || !status) {
      return console.log(err || 'missing response')
    }
    playerCtrl.updateCastStatus(status)
  }
}

export default CastManager
