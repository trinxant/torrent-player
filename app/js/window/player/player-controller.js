import PlayerView from './player-view'
import CastManager from './cast-manager'
import OpenSubs from 'opensubtitles-api'
const remote = window.require('electron').remote

class PlayerController {

  constructor () {
    this._playerView = new PlayerView()
    this._openSubtitlesSetup()
    this._setUpViewEvents()
    this._setUpKeyEvents()
    this._currentFile = null
    this._currentTorrent = null
    this._castManager = new CastManager()
    this._ccPlayer = null
    this._state = {
      volume: 0,
      playing: true,
      fullscreen: false,
      currentTime: 0,
      streamming: false,
      casting: false
    }
  }

  start () {
    this.hide()
    this._setTimer()
    this._setupCast()
  }

  _setUpViewEvents () {
    this._playerView.on('volume-change', (percentage) => {
      this._state.volume = percentage
    })
    this._playerView.on('loaded-metadata', () => {})
    this._playerView.on('stop', () => {
      if (this._state.casting) {
        this._castManager.stop()
        this._state.casting = false
      }
    })
    this._playerView.on('play-pause', () => {
      if (this._state.casting) {
        this._state.playing ?
          this._castManager.pause() :
          this._castManager.resume()
        this._state.playing = !this._state.playing
        this._playerView.setPlayButton(this._state.playing)
      }
    })
    this._playerView.on('play', () => {
      this._state.playing = true
      if (this._state.casting) this._castManager.resume()
    })
    this._playerView.on('pause', () => {
      this._state.playing = false
      if (this._state.casting) this._castManager.resume()
    })
    this._playerView.on('cast-click', () => {
      this._castClick()
    })
    this._playerView.on('subs-click', () => {
      this._subsClick()
    })
    this._playerView.on('subtitles-clicked', (lang) => {
      if (lang === 'off') {
        this._playerView.setSubtitles(lang)
        return
      }
      window.subsManager.downloadSubtitles(lang)
    })
    window.subsManager.on('subtitles-ready', (params) => { this._playerView.setSubtitles(params.lang, params.path) })
    this._playerView.on('device-clicked', (deviceId) => {
      this._cast(this._castInfo.networkURL, deviceId)
    })
    this._playerView.on('player-rendered', () => {
      if (this._castManager.getDevices().length > 0) {
        this._playerView.showCastBtn()
      }
    })
    this._playerView.on('cast-seek', seconds => {
      this._castManager.seek(seconds)
    })
  }

  _downloadSubtitles (url, callback) {
    this.downloadFile(url, 'subtitles.srt')
  }

  _castClick () {
    if (this._state.casting) {
      this._castManager.stop()
      this._state.casting = false
      this._playerView.exitCastMode()
      this._playerView.seek(null, this._state.currentTime - 8)
      this._playerView.play()
      return
    }
    this._playerView.renderDevicesPopup(this._arrangeCastData())
    this._playerView.showPopup('devices')
    let timeoutID = setTimeout(() => {
      this._playerView.hidePopup('devices')
      clearTimeout(timeoutID)
    }, 3000)
  }

  _subsClick () {
    this._playerView.showPopup('subtitles')
    let subtitlesTimeout = setTimeout(() => {
      this._playerView.hidePopup('subtitles')
      clearTimeout(subtitlesTimeout)
    }, 3000)
  }

  _arrangeCastData () {
    let output = []
    this._castManager.getDevices().forEach((player) => {
      output.push(player.name)
    })
    return output
  }

  _setUpKeyEvents () {
    document.addEventListener('keydown', (event) => {
      if (!this._state.casting) this._playerView.handleControlsToggle()
      if (event.ctrlKey) {
        switch (event.which) {
          case '':
            break
        }
      } else {
        switch (event.which) {
          case 32:
            this._playerView.playPause()
            break
          case 38:
            this._state.volume += 10
            this._playerView.volumeChange(0, this._state.volume / 100)
            break
          case 40:
            this._state.volume -= 10
            this._playerView.volumeChange(0, this._state.volume / 100)
            break
          case 39:
            this._playerView.forward(10)
            break
          case 37:
            this._playerView.forward(-10)
            break
          case 27:
            if (remote.getCurrentWindow().isFullScreen()) this._playerView.fullScreenHandler()
            break
        }
      }
    }, false)
  }

  _setupCast () {
    this._castManager.start()
    if (this._castManager.getDevices().length > 0) {
      this._playerView.showCastBtn()
    }
    this._castManager.on('new-device', (device) => {
      this._playerView.showCastBtn()
      this._playerView.renderDevicesPopup(this._arrangeCastData())
    })
    window.torrentManager.on('server-ready', (info) => {
      this._castInfo = info
    })
  }

  _setTimer () {
    window.setInterval(() => {
      if (this._state.streamming) this._updateStreammingInfo()
    }, 1000)
  }

  _updateStreammingInfo () {
    this._playerView.updateDownloadInfo()
  }

  _openSubtitlesSetup () {
    this._OpenSubtitles = new OpenSubs('TFG ls26104')
    this._OpenSubtitles.login()
      .then(res => {
        this._OSToken = res.token
      })
      .catch(err => {
        console.log(err)
      })
  }

  _findSubtitles () {
    window.subsManager.findSubtitles({
      filename: this._currentFile.name
    }, (subtitles) => {
      this._subtitles = subtitles
      console.log(subtitles['en'])
      this._playerView.renderSubtitles(this._subtitles)
    })
  }

  startStream (hash) {
    window.torrentManager.startTorrent(hash, (torrent) => {
      this._currentTorrent = torrent
      this._state.streamming = true
      window.torrentManager.startServer(this._currentTorrent)
      torrent.files.forEach((file) => {
        var fileExt = file.name.substr(file.name.lastIndexOf('.') + 1)
        if (fileExt === 'mp4' || fileExt === 'webm') {
          this.playFile(file)
          this._findSubtitles()
        }
      })
    })
  }

  playFile (file) {
    console.log('file', file)
    this._currentFile = file
    file.renderTo('#torrent-player', (err, elem) => {
      if (err) {
        console.log('Cant stream torent', err)
      } else {
        this._playerView.adaptPlayerSize()
        this._playerView.setTitle(file.name)
      }
    })
  }

  _cast (url, deviceId) {
    this._state.casting = true
    console.log(this._currentTorrent, this._currentFile)
    this._playerView.enterCastMode(this._castManager._ccPlayer.name)
    const completeUrl = url + '/' + this._currentTorrent.mp4Index
    const seconds = this._playerView.getSeconds() > 15 ? this._playerView.getSeconds() : 0
    this._state.currentTime = seconds
    this._castManager.play(completeUrl, this._currentFile.name, seconds - 8, deviceId)
  }

  updateCastStatus (status) {
    console.log(status)
    if (status.playerState === 'PLAYING') {
      this._state.playing = true
    } else {
      this._state.playing = false
    }
    this._playerView.setPlayButton(this._state.playing)
    this._state.currentTime = status.currentTime
    this._playerView.updateTimeSlider(status.currentTime)
  }

  show () {
    this._playerView._playerContainer.show()
  }

  hide () {
    this._playerView._playerContainer.hide()
  }
}

export default new PlayerController()
