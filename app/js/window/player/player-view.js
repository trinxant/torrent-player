import EventEmitter from 'events'
const electron = window.require('electron')
import Mustache from 'mustache'
const remote = electron.remote
import htmlToElement from 'html-to-element'
import $ from 'jquery'

class PlayerView extends EventEmitter {
  constructor (playerElement) {
    super()
    const playerHTML = "<video width='100%' id='torrent-player'></video>"
    this._playerElement = htmlToElement(playerHTML)
    this._playerContainer = $('.player')

    this._casting = false

    // Buttons
    this._playButton = null
    this._muteButton = null
    this._fullScreenButton = null

    this._mouseMoveTimeout

    // Vars
    this._firstClick = false
    this._loadedMetadata = false

    // Sliders
    this._seekBar = null
    this._volumeBar = null
    this._volumeDrag = false

    this._loadControlsTemplate('controls')
  }

  _renderPlayer () {
    this._playerContainer.prepend(this._playerElement)
    this._setLoader()
  }

  _renderControls () {
    $('#video-header').remove()
    $('#video-controls').remove()
    let controlsHTML = Mustache.render(this._controlsTpl)
    this._playerContainer.append(htmlToElement(controlsHTML))
  }

  _setLoader () {
    this._loaderElement = htmlToElement("<div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>")
    $('.player').append(this._loaderElement)
  }

  renderDevicesPopup (devices) {
    let html
    $('.devices-popup').html('')
    devices.forEach((name, i) => {
      html = '<div class="popup-item" ' + 'id="device' + i + '"' + '>' + name + '</div>'
      $('.devices-popup').append(html)
    })
  }

  renderSubtitles (subtitles) {
    let html
    let sub
    $('.subtitles-popup').html('')
    $('.subtitles-popup').append('<div class="popup-item active" id="subtitle-off">Off</div>')
    Object.keys(subtitles).forEach((lang) => {
      sub = subtitles[lang]
      html = '<div class="popup-item" ' + 'id="subtitle-' + sub.langcode + '"' + '>' + sub.lang.split(' ')[0] + '</div>'
      $('.subtitles-popup').append(html)
    })
  }

  showDevicecsPopup () {
    $('.devices-popup').show()
  }

  _loadControlsTemplate (name) {
    $.get(`assets/${name}.mst`, (template) => {
      this._controlsTpl = template
      this._setUpPlayer()
    })
  }

  _setUpPlayer () {
    this._renderPlayer()
    this._renderControls()
    this._resetControls()
    this._winSizes = remote.getCurrentWindow().getContentSize()
    this.emit('player-rendered')
    $('.exit-player').click(() => { this.closePlayer() })
    this._playerElement.onloadedmetadata = () => {
      this._loadedMetadata = true;
      this.emit('loaded-metadata')
      $(this._loaderElement).remove()
      remote.getCurrentWindow().setMinimumSize(350, 200)
      this._setUpControls()
      this._setTimer()
      this.showVideo()
      this.adaptPlayerSize()
      this.volumeChange(0, 1)
    }
  }

  _setTimer () {
    if (this._interval) clearInterval(this._interval)
    this._interval = setInterval(() => {
      this.updateTimeSlider(this._playerElement.currentTime)
    }, 1000)
  }

  _setUpControls () {
    // Buttons
    this._setPlayPauseEvent()
    this._setStopEvent()
    this._muteButton = document.getElementById('mute')
    this._fullScreenButton = $('.full-screen').click(this.fullScreenHandler)
    $('.control-cast').click(() => {
      if (this._casting) {
        this._casting = false
        this._setTimer()
      }
      this.emit('cast-click')
    })
    $('.control-subs').click(() => {
      this.emit('subs-click')
    })
    $('.devices-popup').delegate('.popup-item', 'click', (e) => {
      this.hidePopup('devices')
      this._casting = true
      clearInterval(this._mouseMoveTimeout)
      clearInterval(this._interval)
      this.updateTimeSlider(0)
      this.emit('device-clicked', parseInt(e.target.id.slice(-1)))
    })
    $('.subtitles-popup').delegate('.popup-item', 'click', (e) => {
      this.hidePopup('subtitles')
      this.emit('subtitles-clicked', e.target.id.split('-')[1])
    })
    // $('#torrent-player').click(this.playPause)
    $('#torrent-player').dblclick(this.fullScreenHandler)

    // Sliders
    this._seekBar = $('#seek-bar')
    this._volumeBar = $('.volume-range')
    this._volumeProgress = $('.range-progress')
    this._setUpBars()
  }

  _resetControls () {
    this.setTitle('')
    $('.time-left').text('00:00')
    $('.time-right').text('00:00')
    this.seek(0, 0)
    this.volumeChange(0, 0)
    // this.pause()
  }

  showControls () {
    $('#video-controls').addClass('visible')
    $('#video-header').addClass('visible')
  }

  showCastBtn () {
    $('.control-cast').show()
  }

  hideControls () {
    $('#video-controls').removeClass('visible')
    $('#video-header').removeClass('visible')
  }

  fullScreenHandler () {
    const win = remote.getCurrentWindow()
    if (!win.isFullScreen()) {
      win.setAspectRatio(0)
      win.setFullScreen(true)
    } else {
      win.setFullScreen(false)
    }
  }

  closePlayer () {
    this.emit('stop')
    this.exitCastMode()
    clearInterval(this._interval)
    this._resetPlayer()
    this._casting = false
    this._playerContainer.hide()
    $('.free-mode, .search-popup').show()
    remote.getCurrentWindow().setAspectRatio(0)
    remote.getCurrentWindow().setMinimumSize(1030, 550)
    remote.getCurrentWindow().setContentSize(this._winSizes[0], this._winSizes[1], true)
    this.hideVideo()
    if (remote.getCurrentWindow().isFullScreen()) remote.getCurrentWindow().setFullScreen(false)
  }

  _resetPlayer () {
    this._resetControls()
    $(this._playerElement).find('track').remove()
  }

  setTitle (title) {
    $('.player-title').text(title)
  }

  volumeChange (x, vol) {
    var volume = $('.volume-range')
    var percentage

    if (vol) {
      percentage = vol * 100
    } else {
      const position = x - volume.offset().left
      percentage = 100 * position / volume.width()
    }

    if (percentage < 0) {
      percentage = 0
    }
    if (percentage > 100) {
      percentage = 100
    }
    this._changeVolumeIcon(percentage)
    this._playerElement.volume = percentage / 100
    $('.range-progress').css('width', percentage + '%')
    this.emit('volume-change', percentage)
  }

  forward (seconds) {
    this.seek(null, this._playerElement.currentTime + seconds)
  }

  seek (x, sec) {
    let duration = this._playerElement.duration
    if (isNaN(duration)) duration = 0
    const progress = $('.progress-range')
    const position = x - progress.offset().left
    let percentage = 100 * position / progress.width()
    let seconds = percentage * duration / 100
    if (typeof sec !== 'undefined') {
      seconds = sec
      percentage = seconds * 100 / duration
    }
    if (isNaN(percentage)) percentage = 0
    this._casting ?
      this.emit('cast-seek', seconds) :
      this._playerElement.currentTime = seconds
    this.updateTimeSlider(seconds)
  }

  updateTimeSlider (seconds) {
    let duration = this._playerElement.duration
    let percentage = seconds * 100 / duration
    $('.progress-progress').css('width', percentage + '%')
    $('.time-left').text(this._secToMin(seconds))
    duration = this._secToMin(duration)
    if (duration === 'NaN') duration = '-'
    $('.time-right').text(duration)
  }

  setSubtitles (lang, src) {
    $('.subtitles-popup').find('.popup-item').removeClass('active')
    let found = false
    const tracks = this._playerElement.textTracks
    for (let i = 0; i < tracks.length; i++) {
      const track = tracks[i]
      track.mode = 'hidden'
      if (track.id === 'subtitles-' + lang) {
        track.mode = 'showing'
        found = true
      }
    }
    if (!found && lang !== 'off') {
      let trackElem = htmlToElement(`<track kind='subtitles' id="subtitles-${lang}" src='${src}' srclang='${lang}' />`)
      $(this._playerElement).append(trackElem)
      tracks[tracks.length - 1].mode = 'showing'
    }
    $('.subtitles-popup').find('#subtitle-' + lang).addClass('active')
  }
  _secToMin (seconds) {
    let output = ''
    let minutes = '' + Math.floor(seconds / 60)
    if (minutes === '0') minutes = '00'
    if (minutes.length === 1) minutes = '0' + minutes
    if (minutes.length === 'NaN') minutes = '-'
    output += minutes
    output += ':'
    let sec = '' + Math.floor(seconds % 60)
    if (sec === '0') sec = '00'
    if (sec.length === 1) sec = '0' + sec
    if (sec.length === 'NaN') sec = '-'
    output += sec
    return output
  }

  _changeVolumeIcon (percentage) {
    $('.volume-off').hide()
    $('.volume-down').hide()
    $('.volume-up').hide()
    if (percentage <= 33.3334) $('.volume-off').show()
    else if (percentage <= 66.6667) $('.volume-down').show()
    else $('.volume-up').show()
  }

  _setUpBars () {
    $('.volume-dummy').on('mousedown', (e) => {
      this._volumeDrag = true
      this._playerElement.mute = false
      this.volumeChange(e.pageX)
    })
    $('.progress-dummy').on('mousedown', (e) => {
      this._progressDrag = true
      this.seek(e.pageX)
    })
    $(document).on('mouseup', (e) => {
      if (this._volumeDrag) {
        this._volumeDrag = false
        this.volumeChange(e.pageX)
      }
      if (this._progressDrag) {
        this._progressDrag = false
        this.seek(e.pageX)
      }
    })
    $(document).on('mousemove', (e) => {
      if (this._volumeDrag) {
        this.volumeChange(e.pageX)
      }
      if (this._progressDrag) {
        this.seek(e.pageX)
      }
      if (!this._casting) this.handleControlsToggle()
    })
    this.volumeChange(0, this._playerElement.volume)
    this.seek(0, 0)
  }

  getSeconds () {
    return this._playerElement.currentTime
  }

  handleControlsToggle () {
    clearTimeout(this._mouseMoveTimeout)
    this.showControls()
    $('.player').find('*').css('cursor', 'default')
    this._mouseMoveTimeout = setTimeout(() => {
      $('.player').find('*').css('cursor', 'none')
      this.hideControls()
    }, 3000)
  }

  enterCastMode (deviceName) {
    this._playerElement.pause()
    $('.casting-panel').find('p').html('Casting to: ' + deviceName)
    $('.full-screen').hide()
    $('.control-cast').addClass('casting')
    $('.casting-panel').css('display', 'flex')
    $('#video-controls').toggleClass('casting')
  }

  exitCastMode () {
    this._casting = false
    $('.casting-panel').css('display', 'none')
    $('.full-screen').show()
    $('.control-cast').removeClass('casting')
    $('#video-controls').toggleClass('casting')
  }

  updateDownloadInfo (info) {
    return
  }

  _setPlayPauseEvent () {
    $('.play-pause').click(() => {
      this.emit('play-pause')
      this.playPause()
    })
  }

  _setStopEvent () {
    $('.stop.control').click(() => {
      this.emit('stop')
      this.closePlayer()
    })
  }

  playPause () {
    if (this._casting) return
    if (this._playerElement.paused === true) {
      this.play()
    } else {
      this.pause()
    }
  }

  setPlayButton (playing) {
    if (playing) {
      $('.play').hide()
      $('.pause').show()
    } else {
      $('.play').show()
      $('.pause').hide()
    }
  }

  play () {
    $('.play').hide()
    $('.pause').show()
    if (!this._casting) this._playerElement.play()
    this.emit('play')
  }

  pause () {
    $('.play').show()
    $('.pause').hide()
    if (!this._casting) this._playerElement.pause()
    this.emit('pause')
  }

  adaptPlayerSize () {
    let width = this._playerElement.videoWidth
    let height = this._playerElement.videoHeight
    var aspect = width / height
    var screenSize = electron.screen.getPrimaryDisplay().size
    let winWidth = Math.round(screenSize.width * 0.8)
    let winHeight = Math.round(winWidth / aspect)
    remote.getCurrentWindow().setSize(winWidth, winHeight, true)
    remote.getCurrentWindow().setAspectRatio(aspect)
  }

  showVideo () { $('#torrent-player').show() }
  hideVideo () { $('#torrent-player').hide() }
  showPopup (type) {
    if (type === 'devices') $('.devices-popup').show()
    if (type === 'subtitles') $('.subtitles-popup').show()
  }
  hidePopup (type) {
    if (type === 'devices') $('.devices-popup').hide()
    if (type === 'subtitles') $('.subtitles-popup').hide()
  }

}

export default PlayerView
